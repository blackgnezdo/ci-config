# GHC GitLab CI Worker
# ====================
#
# Usage:
#
# 1. Generate a runner token using `gitlab-runner register`.
#
# 2. Drop this file in /etc/nixos.
#
# 3. Add the following to the import list of /etc/nixos/configuration.nix:
#
#      (import ./ghc-ci-worker.nix {
#         name = ="ghc-ci-1";
#         cores = 48;
#         token = "TOKEN";
#       })
#
# 4. Build and switch to the configuration,
#
#      nixos-rebuild switch
#
#    gitlab-runner will likely fail spuriously.
#
# 5. Restart gitlab-runner,
#
#      systemctl restart gitlab-runner

{ name, token, cores, lintToken ? null }:
{ pkgs, lib, ... }:

let
  gitlabUrl = "https://gitlab.haskell.org/";

  mkRunnerService = {
    name, token, tags,
    dockerImage ? "debian/jessie", dockerVolumes ? [],
    limit, perJobCores, registrationFlags ? [] }:
  {
    "${name}" = {
      registrationConfigFile = pkgs.writeText "${name}-registration" ''
        CI_SERVER_URL="https://gitlab.haskell.org/"
        REGISTRATION_TOKEN="${token}"
      '';
      executor = "docker";
      registrationFlags = [
        #"--registration-token=${token}"
        #"--url=${gitlabUrl}"
        "--output-limit=16000"
      ] ++ registrationFlags;
      tagList = tags;
      inherit limit dockerImage;
      environmentVariables = {
        CPUS = "${toString perJobCores}";
      };
    };
  };
in
{
  environment.systemPackages = with pkgs; [
    htop vim
  ];

  virtualisation.docker = {
    enable = true;
    autoPrune = {
      enable = true;
      flags = [ "--force" "--all" "--volumes" ];
      dates = "daily";
    };
  };

  services.gitlab-runner = {
    enable = true;
    concurrent = cores / 4 + 2;
    services = (mkRunnerService {
      inherit name token;
      perJobCores = 4;
      limit = cores / 4;
      tags = [ "x86_64-linux" ];
    }) // lib.optionalAttrs (lintToken != null) (mkRunnerService {
      name = "${name}-lint";
      token = lintToken;
      perJobCores = 2;
      limit = 4;
      tags = [ "lint" ];
    })
  };
}
