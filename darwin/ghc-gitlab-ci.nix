with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "ghc-gitlab-ci";

  buildInputs =
  [ pkgs.clang_10
    pkgs.gitlab-runner
    pkgs.python3Full
    pkgs.python3Packages.sphinx
    #pkgs.texlive.combined.scheme-full
    (pkgs.texlive.combine
      { inherit (pkgs.texlive)
            scheme-medium collection-xetex fncychap titlesec tabulary varwidth
            framed capt-of wrapfig needspace dejavu-otf helvetic upquote;
      })
    pkgs.dejavu_fonts
    pkgs.wget
    pkgs.cabal-install
    pkgs.haskell.compiler.ghc8101 # update this sometimes
    pkgs.haskellPackages.alex
    pkgs.haskellPackages.happy
    pkgs.haskellPackages.haddock
    pkgs.automake
    pkgs.autoconf
    pkgs.gmp
    pkgs.ncurses
  ];
  
  shellHook =
    let fonts = pkgs.makeFontsConf { fontDirectories = [ pkgs.dejavu_fonts ]; };
    in ''
    export HOME="/Users/builder"
    export PATH="${pkgs.clang_10}/bin:$PATH"
    export FONTCONFIG_FILE=${fonts}
    export CORES=12
    '';
}
