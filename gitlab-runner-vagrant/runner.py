#!/usr/bin/env python3

"""
Vagrant gitlab-runner executor.
(c) 2021 Ben Gamari
"""

import os
import json
from pathlib import Path
import shutil
import subprocess
import sys

__version__ = "0.1"

# Path to vagrant executable
VAGRANT = 'vagrant'

def do_config(args) -> None:
    vagrant_dir = args.vagrant_dir

    config_file = vagrant_dir / "config.json"
    config = json.load(config_file.open('r')) if config_file.is_file() else {}

    home = Path('/usr/home/vagrant')
    project_dir = Path(os.environ['CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID']) / Path(os.environ['CUSTOM_ENV_CI_PROJECT_PATH_SLUG'])
    builds_dir = home / "builds" / project_dir
    cache_dir = home / "cache" / project_dir
    config = {
        "builds_dir": str(builds_dir),
        "cache_dir": str(cache_dir),
        "builds_dir_is_shared": True,
        "hostname": "custom-hostname",
        "driver": {
          "name": "Vagrant driver",
          "version": f"v{__version__}"
        },
        "job_env" : {k: str(v) for k,v in config.get('environment', {}).items()},
    }
    json.dump(config, sys.stdout)

def get_build_dir(vagrant_dir: Path) -> Path:
    return vagrant_dir / "builds" / get_machine_id()

def get_machine_id() -> str:
    env = lambda k: os.environ[f'CUSTOM_ENV_CI_{k}']

    return '-'.join([
        f"runner-{env('RUNNER_ID')}",
        f"project-{env('PROJECT_ID')}",
        f"concurrent-{env('CONCURRENT_PROJECT_ID')}",
        f"job-{env('JOB_ID')}",
    ])

def do_prepare(args) -> None:
    vagrant_dir = args.vagrant_dir
    build_dir = get_build_dir(vagrant_dir)
    build_dir.mkdir(parents=True, exist_ok=True)

    vagrantfile = vagrant_dir / 'Vagrantfile'
    (build_dir / 'Vagrantfile').symlink_to(vagrantfile)

    subprocess.run(
        [VAGRANT, 'up', '--provision'],
        cwd=build_dir,
        check=True
    )

def do_run(args) -> None:
    vagrant_dir = args.vagrant_dir
    script = args.script
    stage = args.stage
    build_dir = get_build_dir(vagrant_dir)

    print('run', stage)
    subprocess.run(
        [VAGRANT, 'ssh'],
        cwd=build_dir,
        stdin=script.open('r'),
        check=True
    )

def do_cleanup(args) -> None:
    vagrant_dir = args.vagrant_dir
    build_dir = get_build_dir(vagrant_dir)

    print("cleanup")
    subprocess.run(
        [VAGRANT, 'destroy', '--force'],
        cwd=build_dir,
        check=True
    )

    #shutil.rmtree(build_dir)

def main() -> None:
    import argparse
    parser = argparse.ArgumentParser()
    subparser = parser.add_subparsers()

    def mode(name: str, func):
        p = subparser.add_parser(name)
        p.set_defaults(func=func)
        p.add_argument('vagrant_dir', type=Path, help='Vagrant directory')
        return p

    p = mode('config', do_config)
    p = mode('prepare', do_prepare)
    p = mode('run', do_run)
    p.add_argument('script', type=Path, help='Script to run')
    p.add_argument('stage', type=str, help='Stage')
    p = mode('cleanup', do_cleanup)

    args = parser.parse_args()
    args.func(args)

if __name__ == '__main__':
    main()

