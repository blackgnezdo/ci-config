#!/usr/bin/env bash

set -e

if [ -z "$REG_TOKEN" ]; then
  echo "error: REG_TOKEN is not set."
  exit 1
fi

if [ -z "$VAGRANT_RUNNER_DIR" ]; then
  echo "error: VAGRANT_RUNNER_DIR is not set."
  exit 1
fi

if [ ! -f "$VAGRANT_RUNNER_DIR/Vagrantfile" ]; then
  echo "error: VAGRANT_RUNNER_DIR does not contain a Vagrantfile."
  exit 1
fi

if [ ! -f "$VAGRANT_RUNNER_DIR/runner-config.sh" ]; then
  echo "error: VAGRANT_RUNNER_DIR does not contain a runner-config.sh."
  exit 1
fi

(cd "$VAGRANT_RUNNER_DIR"; vagrant validate )

# Must set $RUNNER_TYPE and RUNNER_TAGS
source "$VAGRANT_RUNNER_DIR/runner-config.sh"

if [ -z "$RUNNER_TYPE" ]; then
  echo "error: RUNNER_TYPE was not set by runner-config.sh."
  exit 1
fi

if [ -z "$RUNNER_TAGS" ]; then
  echo "error: RUNNER_TAGS was not set by runner-config.sh."
  exit 1
fi

bin="$(realpath $(dirname "$0"))/runner.py"
dir="$(realpath "$VAGRANT_RUNNER_DIR")"

gitlab-runner register \
  --non-interactive \
  --name="$(hostname)-vagrant-$RUNNER_TYPE" \
  --url="https://gitlab.haskell.org/" \
  --tag-list="$RUNNER_TAGS" \
  --registration-token="$REG_TOKEN" \
  --executor=custom \
  --output-limit=16000 \
  --custom-config-exec="$bin" \
  --custom-config-args="config" \
  --custom-config-args="$dir" \
  --custom-prepare-exec="$bin" \
  --custom-prepare-args="prepare" \
  --custom-prepare-args="$dir" \
  --custom-run-exec="$bin" \
  --custom-run-args="run" \
  --custom-run-args="$dir" \
  --custom-cleanup-exec="$bin" \
  --custom-cleanup-args="cleanup" \
  --custom-cleanup-args="$dir"
